using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyTrigger : MonoBehaviour
{
	[SerializeField]
	private Collider2D key = null;

	[SerializeField]
	private GameObject triggerTarget = null;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision == key)
		{
			key.attachedRigidbody.isKinematic = true;
			key.attachedRigidbody.velocity = Vector2.zero;
			key.attachedRigidbody.angularVelocity = 0;
			key.transform.parent = transform;
			key.transform.localPosition = Vector3.zero;
			triggerTarget.GetComponent<ITriggerReciever>().Trigger();
		}
	}
}
