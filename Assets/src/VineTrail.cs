using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VineTrail : MonoBehaviour
{
	private Path trailPath = new Path();
	public Path TrailPath => trailPath;

	private float traceThreshold = 0.01f;

	private List<Transform> vineNodes = new List<Transform>();

	[SerializeField]
	private float nodeLength = 0.5f;

	[SerializeField]
	private Transform vinePrefab;
	public Transform VinePrefab => vinePrefab;

	[SerializeField]
	private Sprite vineRootGFX;

	[SerializeField]
	private Sprite vineGreenGFX;

	private bool underground;

	private float maxLength = 50;

	// --------------------------------------------------------------------------------------------

	private void Start()
	{
		TracePathAt(transform.position, true);
	}

	private void Update()
	{
		AttachVinesToPath();
	}

	// --------------------------------------------------------------------------------------------

	public void TracePathAt(Vector2 pos, bool underGround)
	{
		if (trailPath.IsEmpty) trailPath.AddPoint(transform.position, underground);

		Vector2 dif = pos - trailPath.LastPos;
		if(dif.magnitude > traceThreshold)
		{
			PlacePathNode(pos, underGround);
		}

		underground = underGround;
	}

	public void SetLastNode(Vector2 pos, bool underGround)
	{
		var lastNode = trailPath.path.Last;
		var prevNode = lastNode.Previous;

		Vector2 dif = lastNode.Value.Item1 - prevNode.Value.Item1;
		float targLen = trailPath.PathLength - dif.magnitude;

		Vector2 dif2 = pos - prevNode.Value.Item1;
		targLen += dif2.magnitude;

		trailPath.path.RemoveLast();
		trailPath.path.AddLast(new LinkedListNode<Tuple<Vector2, bool>>(new Tuple<Vector2, bool>(pos, underground)));

		trailPath.SetPathLength(targLen);

		while (trailPath.PathLength > maxLength)
		{
			trailPath.RemoveLengthFromStart(trailPath.PathLength - maxLength);
		}
	}

	// --------------------------------------------------------------------------------------------

	private void PlacePathNode(Vector2 pos, bool underGround)
	{
		trailPath.AddPoint(pos, underGround);

		while(trailPath.PathLength > maxLength)
		{
			trailPath.RemoveLengthFromStart(trailPath.PathLength - maxLength);
		}
	}

	private void AttachVinesToPath()
	{
		float distance = 0;
		int nodeIndex = 0;
		while (distance <= trailPath.PathLength)
		{
			Tuple<Vector2, bool> lPos = trailPath.GetPointAtDistance(distance);
			distance += nodeLength;
			Tuple<Vector2, bool> ePos = trailPath.GetPointAtDistance(distance);

			while(nodeIndex >= vineNodes.Count)
			{
				vineNodes.Add(Instantiate(vinePrefab));
				vineNodes[nodeIndex].parent = transform;
			}

			Vector2 dif = ePos.Item1 - lPos.Item1;
			Transform curSeg = vineNodes[nodeIndex];
			curSeg.position = ePos.Item1;
			curSeg.right = dif;

			SpriteRenderer sr = curSeg.GetComponent<SpriteRenderer>();
			if (lPos.Item2 || ePos.Item2)
			{
				sr.sprite = vineRootGFX;
				sr.color = Color.gray;
			}
			else
			{
				sr.color = Color.white;
				sr.sprite = vineGreenGFX;
			}

			curSeg.localScale = new Vector3(dif.magnitude * 2, dif.magnitude * 2, 1);

			nodeIndex++;
		}
	}

	// --------------------------------------------------------------------------------------------

	public class Path
	{
		public bool IsEmpty => path.Count <= 0;

		public Vector2 LastPos => path.Last.Value.Item1;

		public LinkedList<Tuple<Vector2, bool>> path = new LinkedList<Tuple<Vector2, bool>>();

		private float pathLength = 0;

		public float PathLength => pathLength;

		public void AddPoint(Vector2 point, bool underground)
		{
			if (!IsEmpty)
			{
				pathLength += (point - LastPos).magnitude;
			}

			path.AddLast(new Tuple<Vector2, bool>(point, underground));
		}

		public void RemoveFirstPoint()
		{
			if (IsEmpty) return;
			var node = path.First;
			var prevNode = node.Next;

			if(prevNode != null)
			{
				pathLength -= (node.Value.Item1 - prevNode.Value.Item1).magnitude;
			}
			else pathLength = 0;

			path.RemoveFirst();
		}

		public void RemoveLengthFromStart(float length)
		{
			if(pathLength - length < 0)
			{
				pathLength = 0;
				var node = path.First;
				while (node.Next != null)
				{
					node = node.Next;
					path.RemoveFirst();
				}
				return;
			}

			float dist = length;
			while(dist > 0)
			{
				var fnode = path.First;
				var nextNode = fnode.Next;

				Vector2 dif = nextNode.Value.Item1 - fnode.Value.Item1;
				if(dist - dif.magnitude < 0)
				{
					Vector2 targetPos = fnode.Value.Item1 + dif.normalized * dist;
					pathLength -= dist;
					path.RemoveFirst();
					path.AddFirst(new LinkedListNode<Tuple<Vector2, bool>>(new Tuple<Vector2, bool>(targetPos, fnode.Value.Item2)));
					break;
				}

				dist -= dif.magnitude;
				pathLength -= dif.magnitude;
				path.RemoveFirst();
			}
		}

		public Tuple<Vector2, bool> GetPointAtDistance(float dist)
		{
			var node = path.First;
			if (node == null) return null;

			while(node.Next != null)
			{
				var prevNode = node;
				node = node.Next;

				Vector2 dif = node.Value.Item1 - prevNode.Value.Item1;
				if(dist - dif.magnitude < 0)
				{
					return new Tuple<Vector2, bool>(Vector2.Lerp(prevNode.Value.Item1, node.Value.Item1, dist / dif.magnitude), node.Value.Item2);
				}
				dist -= dif.magnitude;
			}

			return new Tuple<Vector2, bool>(path.Last.Value.Item1, path.Last.Value.Item2);
		}

		public void SetPathLength(float newLen)
		{
			pathLength = newLen;
		}
	}
}