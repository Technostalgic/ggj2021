using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
	[SerializeField]
	AudioSource music;

	bool fade = false;

	private void Start()
	{
		PlayerController.Instance.receivingInput = false;
	}

	private void Update()
	{
		if (Input.GetMouseButton(0))
		{
			fade = true;
		}

		if (fade) {
			SpriteRenderer sr = GetComponent<SpriteRenderer>();
			sr.color = new Color(1, 1, 1, sr.color.a - Time.deltaTime);
			if(sr.color.a <= 0)
			{
				music.Play();
				PlayerController.Instance.receivingInput = true;
				gameObject.SetActive(false);
			}
		}


	}
}
