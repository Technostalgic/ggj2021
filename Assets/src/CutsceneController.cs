using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CutsceneController : MonoBehaviour
{
	public static CutsceneController instance;

	[SerializeField]
	private Transform everything = null;

	[SerializeField]
	private SpriteRenderer black = null;

	[SerializeField]
	private SpriteRenderer cutsceneSprite = null;

	[SerializeField]
	private SpriteRenderer prevCutsceneSprite = null;

	[SerializeField]
	private Sprite[] cutScenes = null;

	[SerializeField]
	private SpriteRenderer[] spriteStack = null;

	private int curCutscene = 0;

	public UnityEvent OnFadeBlack = new UnityEvent();
	public UnityEvent OnFinish = new UnityEvent();

	private bool fadingStack = false;
	private bool fadingBlack = false;
	private float fadeStart = 0;
	private bool fadeBack = true;

	public void DoCutscene(int id, bool fadeback = true)
	{
		fadeBack = fadeback;
		curCutscene = id;
		OnFadeBlack.AddListener(StartCutscene);
		FadeToBlack();
	}

	public void StartCutscene()
	{
		for (int i = 1; i < 3; i++)
		{
			spriteStack[i].color = Color.white;
		}
		spriteStack[3].color = Color.black;
		spriteStack[1].sprite = spriteStack[2].sprite;
		spriteStack[2].sprite = cutScenes[curCutscene];
		FadeAwayStack();
	}

	private void FadeToBlack()
	{
		fadeStart = Time.time;
		fadingBlack = true;
		black.enabled = true;
	}

	private void FadeFromBlack()
	{
		fadeStart = -Time.time;
		fadingBlack = true;
		black.enabled = true;
	}

	private void FadeAwayStack()
	{
		fadingStack = true;
		fadeStart = Time.time;
	}

	[ContextMenu("Test")]
	public void Test()
	{
		DoCutscene(1);
	}

	private void Awake()
	{
		instance = this;
		cutsceneSprite.sprite = cutScenes[0];
	}

	private void Update()
	{
		if (fadingBlack) HandleBlackFade();
		if (fadingStack) HandleStackFade();
	}

	private void HandleBlackFade()
	{
		float fadeSpeed = 1;

		float delta = (Time.time - Math.Abs(fadeStart)) / fadeSpeed;

		if (Mathf.Sign(fadeStart) < 0)
		{
			delta = 1 - delta;
		}

		float alpha = Math.Min(delta, 1);

		Color col = black.color;
		col.a = alpha;

		black.color = col;
		if(Mathf.Abs(delta) > 1)
		{
			fadingBlack = false;
			OnFadeBlack.Invoke();
			OnFadeBlack.RemoveAllListeners();
		}
	}

	private void HandleStackFade()
	{
		float fadeSpeed = 1;
		float delta = (Time.time - Math.Abs(fadeStart)) / fadeSpeed;

		if(delta < 1)
		{
			Color col = spriteStack[0].color;
			float tdelta = delta;
			col.a = 1 - tdelta;
			spriteStack[0].color = col;
		}
		else if (delta < 2)
		{
			Color col = spriteStack[1].color;
			float tdelta = delta - 1;
			col.a = 1 - tdelta;
			spriteStack[1].color = col;
			spriteStack[0].color = Color.clear;
		}
		else if (delta < 3)
		{
			if (fadeBack)
			{
				Color col = spriteStack[2].color;
				float tdelta = delta - 2;
				col.a = 1 - tdelta;
				spriteStack[2].color = col;
				spriteStack[0].color = Color.clear;
				spriteStack[1].color = Color.clear;
			}
		}
		else if (delta < 4)
		{
			if (fadeBack)
			{
				Color col = spriteStack[3].color;
				float tdelta = delta - 3;
				col.a = 1 - tdelta;
				spriteStack[3].color = col;
				spriteStack[0].color = Color.clear;
				spriteStack[1].color = Color.clear;
				spriteStack[2].color = Color.clear;
			}
		}
		else
		{
			fadingStack = false;
			OnFinish.Invoke();
			OnFinish.RemoveAllListeners();
			if (!fadeBack) return;
			spriteStack[0].color = Color.clear;
			spriteStack[1].color = Color.clear;
			spriteStack[2].color = Color.clear;
			spriteStack[3].color = Color.clear;
		}
	}
}
