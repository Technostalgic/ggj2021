using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour, ITriggerReciever
{
	[SerializeField]
	private Vector2 translation = Vector2.zero;

	[SerializeField]
	private float rotation = 0;

	private Vector3 startPos = default;

	private float startRotScalar = 0;

	private Quaternion startRot = default;

	private bool isTriggered = false;

	private float triggerTime = 0;

	public void Trigger()
	{
		if (isTriggered) return;
		
		startPos = transform.position;
		startRot = transform.rotation;
		startRotScalar = transform.rotation.eulerAngles.z;

		// transform.position += (Vector3) translation;
		// transform.Rotate(new Vector3(0, 0, rotation));

		isTriggered = true;
		triggerTime = Time.time;
	}

	private void Update()
	{
		if (!isTriggered) return;

		float animLength = 1.5f;
		float delta = (Time.time - triggerTime) / animLength;
		
		Vector3 targetPos = (Vector2)startPos + translation;
		targetPos.z = transform.position.z; 
		Quaternion targetRot = Quaternion.Euler(0, 0, startRotScalar + rotation);

		if (delta >= 1)
		{
			transform.position = targetPos;
			transform.rotation = targetRot;
			return;
		}


		transform.position = Vector3.Lerp(startPos, targetPos, delta);
		transform.rotation = Quaternion.Lerp(startRot, targetRot, delta);
	}
}
