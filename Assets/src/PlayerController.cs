using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
class PlayerController : MonoBehaviour
{
	private static PlayerController instance;
	public static PlayerController Instance => instance;

	private bool isDigging = false;

	private bool isVeryDigging = false;

	private Vector2 mouseDelta = Vector2.zero;

	private Collider2D[] colQuery = new Collider2D[10];

	private Vector2 lastDigPos = Vector2.zero;

	private Rigidbody2D rb2d = null;

	private Transform vinePart = null;

	private Collider2D previousGround = null;

	private Grabbable grabbed = null;

	private bool isReplanting = false;

	private float replantSpeed = 1;

	private Vector2 lastDirectionAnchor = default;

	[SerializeField]
	private ContactFilter2D digContactFilter = default;

	[SerializeField]
	private ContactFilter2D interactibleFilter = default;

	[SerializeField]
	public float maxSoilDistance = 1;

	[SerializeField]
	private float travelSpeed = 3;

	[SerializeField]
	private VineTrail vineTrail;

	[SerializeField]
	public bool receivingInput = true;

	[SerializeField]
	private float maxSpeed = 10;

	// ------------------------------------------------------------------------------------------------

	private void Awake()
	{
		instance = this;
		vineTrail.transform.position = transform.position;
	}

	private void Start()
	{
		lastDirectionAnchor = transform.position;
		vinePart = Instantiate(vineTrail.VinePrefab);
		vinePart.gameObject.SetActive(false);
		rb2d = GetComponent<Rigidbody2D>();
		lastDigPos = transform.position;
		Cursor.lockState = CursorLockMode.Locked;
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();

		CaptureMouseDelta();
		if (isDigging) vineTrail.TracePathAt(transform.position, isVeryDigging);
		else vineTrail.SetLastNode(transform.position, false);
		HandleAboveGroundVinePart();

		if (Input.GetMouseButtonDown(0))
		{
			Grab();
			HandleGrabbing();
		}
		else if (Input.GetMouseButton(0))
		{
			HandleGrabbing();
		}
		else
		{
			UnGrab();
		}

		if (isReplanting) HandleReplant();

		HandleRotation();
	}

	private void FixedUpdate()
	{
		HandleDigging();
		ConstrainMovement();
		HandleMovement();
	}

	// ------------------------------------------------------------------------------------------------
	
	public void Replant()
	{
		isReplanting = true;
		replantSpeed = vineTrail.TrailPath.PathLength;
		rb2d.velocity = Vector2.zero;
	}

	// ------------------------------------------------------------------------------------------------

	private void HandleRotation()
	{
		Vector2 dif = (Vector2)transform.position - lastDirectionAnchor;
		if (dif.magnitude <= Mathf.Epsilon) return;

		rb2d.SetRotation(Mathf.Atan2(dif.y, dif.x) * Mathf.Rad2Deg);

		float anchorDist = 0.15f;
		if (dif.magnitude > anchorDist)
		{
			lastDirectionAnchor = (Vector2)transform.position - dif.normalized * anchorDist;
		}
	}

	private void HandleReplant()
	{
		if(vineTrail.TrailPath.PathLength > 0)
		{
			vineTrail.TrailPath.RemoveLengthFromStart(Time.deltaTime * replantSpeed);
		}
		else
		{
			isReplanting = false;
		}
	}

	private void CaptureMouseDelta()
	{
		mouseDelta.x += Input.GetAxis("Mouse X");
		mouseDelta.y += Input.GetAxis("Mouse Y");
	}

	private void HandleDigging()
	{
		float digColRadius = 0.5f;
		bool wasDiggind = isDigging;
		int hits = Physics2D.OverlapCircle(transform.position, digColRadius, digContactFilter, colQuery);

		isDigging = hits > 0;

		if (isDigging)
		{
			lastDigPos = transform.position;
			previousGround = colQuery[0];

			isVeryDigging = false;
			for(int i = hits - 1; i >= 0; i--)
			{
				if (colQuery[i].OverlapPoint(transform.position))
				{
					isVeryDigging = true;
				}
			}
		}
		else
		{
			isVeryDigging = false;
		}
	}

	private void ConstrainMovement()
	{
		Vector2 lastDigDif = (Vector2)transform.position - lastDigPos;
		float maxDist = maxSoilDistance; // - 0.1f;

		if (lastDigDif.magnitude > maxDist)
		{
			rb2d.velocity += mouseDelta * travelSpeed;
			Vector2 targetPos = lastDigPos + lastDigDif.normalized * maxDist;
			transform.position = new Vector3(targetPos.x, targetPos.y, transform.position.z);
		}
	}

	private void HandleMovement()
	{
		if (!receivingInput || isReplanting)
		{
			mouseDelta.x = 0;
			mouseDelta.y = 0;
			rb2d.velocity *= 0.95f;
			return;
		}

		Vector2 lastDigDif = (Vector2)transform.position - lastDigPos;
		if(lastDigDif.magnitude <= maxSoilDistance)
		{
			rb2d.velocity += mouseDelta * travelSpeed;
		}

		mouseDelta.x = 0;
		mouseDelta.y = 0;

		rb2d.velocity *= 0.95f;

		if(rb2d.velocity.magnitude > maxSpeed)
		{
			rb2d.velocity = rb2d.velocity.normalized * maxSpeed;
		}
	}

	private void HandleAboveGroundVinePart()
	{
		if (isDigging)
		{
			vinePart.gameObject.SetActive(false);
			return;
		}

		vinePart.gameObject.SetActive(true);
		vinePart.position = transform.position;

		Vector2 dif = (Vector2)transform.position - vineTrail.TrailPath.LastPos;
		vinePart.localScale = new Vector3(dif.magnitude * 2, dif.magnitude, 1);
		vinePart.right = dif;
	}

	private void HandleGrabbing()
	{
		if(grabbed == null)
		{
			return;
		}

		Vector2 dif = transform.position - grabbed.transform.position;
		grabbed.rb2d.velocity = dif / Time.fixedDeltaTime;
		rb2d.angularVelocity = 0; 

		if (isDigging) UnGrab();
	}

	private void Grab()
	{
		float interactRadius = 0.5f;
		int hitCount = Physics2D.OverlapCircle(transform.position, interactRadius, interactibleFilter, colQuery);
		if (hitCount <= 0) return;

		IInteractible interactible = colQuery[0].gameObject.GetComponent<IInteractible>();
		if (interactible != null)
		{
			interactible.Interact();

			if(grabbed == null)
			{
				if(interactible is Grabbable grabbable)
				{
					grabbed = grabbable;
					Rigidbody2D rb = grabbed.GetComponent<Rigidbody2D>();
				}
			}
		}
	}

	private void UnGrab()
	{
		if (grabbed == null) return;

		Rigidbody2D rb = grabbed.GetComponent<Rigidbody2D>();
		rb.velocity = GetComponent<Rigidbody2D>().velocity;
		grabbed = null;
	}
}