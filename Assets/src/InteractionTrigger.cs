using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionTrigger : MonoBehaviour, IInteractible
{
	[SerializeField]
	public GameObject reciever;

	[SerializeField]
	public bool auto = true;

	// --------------------------------------------------------------------------------------------

	public void Interact()
	{
		ITriggerReciever rec = reciever.GetComponent<ITriggerReciever>();
		if (rec != null) rec.Trigger();
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(auto && collision.GetComponent<PlayerController>() != null)
		{
			Interact();
		}
	}
}