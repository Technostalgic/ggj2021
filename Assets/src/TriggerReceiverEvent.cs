using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerReceiverEvent : MonoBehaviour, ITriggerReciever
{
	private bool triggered = false;

	[SerializeField]
	private UnityEvent onTrigger;

	public void Trigger()
	{
		triggered = true;
		onTrigger.Invoke();
	}
}
