using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
	[SerializeField]
	private float playerMaxGroundDistance = 1.5f;

	[SerializeField]
	private Transform startPosition = null;

	[SerializeField]
	private int finishCutscene = 1;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		PlayerController pc = collision.gameObject.GetComponent<PlayerController>();

		if(pc != null)
		{ 
			Debug.Log("Player entered level: " + name);
			pc.maxSoilDistance = playerMaxGroundDistance;
			if (startPosition != null) pc.transform.position = new Vector3(startPosition.position.x, startPosition.position.y, pc.transform.position.z);
			pc.Replant();
		}
	}

	private void OnTriggerExit2D(Collider2D collision)
	{

		PlayerController pc = collision.gameObject.GetComponent<PlayerController>();

		if (pc != null)
		{
			if(finishCutscene == 3)
			{
				CutsceneController.instance.DoCutscene(finishCutscene, false);
				void blah(){
					CutsceneController.instance.DoCutscene(4, false);
				};
				CutsceneController.instance.OnFinish.AddListener(blah);
			}
			else CutsceneController.instance.DoCutscene(finishCutscene);
		}
	}

	private void OnTriggerStay2D(Collider2D collision)
	{
		PlayerController pc = collision.gameObject.GetComponent<PlayerController>();

		if (pc != null)
		{
			HandleCameraZoom();
		}
	}

	private void HandleCameraZoom()
	{
		Vector2 dif = transform.position - Camera.main.transform.position;
		if (dif.magnitude > 0.1)
		{
			Vector2 targetPos = transform.position;
			Vector3 finalPos = (Vector2)Camera.main.transform.position + dif.normalized * Time.deltaTime * 10;
			finalPos.z = Camera.main.transform.position.z;

			Camera.main.transform.position = finalPos;
		}
	}
}
