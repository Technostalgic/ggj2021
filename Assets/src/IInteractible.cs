using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractible
{
	public void Interact();
}

public interface ITriggerReciever
{
	public void Trigger();
}

#if UNITY_EDITOR
public static class Utility
{
	[UnityEditor.MenuItem("GameObject/MatchBoxCollidersToSprite")]
	public static void ResizeCollidersToSprite()
	{
		Object[] objs = UnityEditor.Selection.objects;
		foreach(var obj in objs)
		{
			if(obj is GameObject go)
			{
				SpriteRenderer sr = go.GetComponent<SpriteRenderer>();
				BoxCollider2D col = go.GetComponent<BoxCollider2D>();
				if(sr != null && col != null)
				{
					col.size = sr.size;
				}
			}
		}
	}
}
#endif