# Roots

## Requirements

* Unity3D - Version 2020.3.41f1 (Must be this exact version) [Can be downloaded here](https://download.unity3d.com/download_unity/7c19dc9acfda/UnityDownloadAssistant-2020.3.41f1.exe)

* Code editor of your choice for C# - (Unity installation includes Visual Studio C# I think - so this should be covered)

* Git - Can be installed from https://git-scm.com/

## Getting The Project up and Running

For these instructions I will assume you are using Windows.

In order to start working on the project you need to clone this git repository from this url (https://gitlab.com/Technostalgic/ggj2023).
To do this, make sure you have git installed. Then create a directory that you want to put the project in. Go into that directory and open a terminal there,
if you have git installed and integrated into explorer, you can just right click in the directory and press "Git Bash Here". Once you have the terminal,
just type `git clone https://gitlab.com/Technostalgic/ggj2023` (or copy and paste it, but be careful - terminal hotkeys for copy and paste are different than ctrl c / v, you 
can right click to copy and paste instead).

once the repository is downloaded, close out of the terminal. You can navigate into the repository and go to `/Assets/Scenes` in that directory there will be a file called
`SampleScene.unity`. This is the main game scene. No I never changed the name it is just the scene that unity auto-generated when I created the project.
If you have unity properly installed, you can just double click on the file and unity will open the project for you.

and viola - there's the project. Enjoy!

To view the source code, you can go to the "Project" tab window (should be on the bottom part of the unity editor window), and right click there and select "Open C# Project".
If you have Visual Studio C# installed and properly set up with unity, it will open Visual C#. Go to the "Solution Explorer" side pane and expand the foldouts there from:
`Solution -> Assembly-CSharp -> Assets -> src` and under that there will be a list of all the source files.

These are C# scripts. In Unity, these kind of C# scripts can be attached to "Game Objects". If you go back into the unity editor and look at the "Hierarchy" tab, on the left, 
you'll see an expandable hierarchy of all the game objects that exist in the scene. You can select them and view the components attached to each one in the "Inspector" tab, 
on the right. These components will include any C# scripts that are on the game objects.